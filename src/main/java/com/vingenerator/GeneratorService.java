package com.vingenerator;

import org.springframework.stereotype.Service;

import java.util.Random;

@Service("generatorService")
public class GeneratorService {

    Random random = new Random();

    public String generateVINCode(){
            int randomNumber = random.nextInt(101);
        return "<here should be shown VIN, but enjoy random [0-100] instead [" + randomNumber + "]>";
    }
}
