package com.vingenerator;

public class VINCodeResponse {
    private final String vin;

    public VINCodeResponse(String vin){
        this.vin = vin;
    }

    public String getVin(){
        return vin;
    }

}
