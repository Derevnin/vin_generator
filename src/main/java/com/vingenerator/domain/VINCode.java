package com.vingenerator.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class VINCode {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    private String VINcode;

    public VINCode() {
    }

    public VINCode(String VINcode) {
        this.VINcode = VINcode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVINcode() {
        return VINcode;
    }

    public void setVINcode(String VINcode) {
        this.VINcode = VINcode;
    }
}
