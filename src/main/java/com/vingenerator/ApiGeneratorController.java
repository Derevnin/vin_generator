package com.vingenerator;

import com.vingenerator.BaseResponse;
import com.vingenerator.GeneratorService;
import com.vingenerator.VINCodeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ApiGeneratorController {

    @Autowired
    GeneratorService generatorService;

    @GetMapping(value = "/generator")
    public ResponseEntity<VINCodeResponse> getVINcode() {
        String generatedCode = generatorService.generateVINCode();
        VINCodeResponse response = new VINCodeResponse(generatedCode);
        return new ResponseEntity<VINCodeResponse>(response, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<BaseResponse> showStatus() {
        BaseResponse response = new BaseResponse("success");
        return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
    }
}
