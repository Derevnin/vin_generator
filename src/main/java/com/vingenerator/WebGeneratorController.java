package com.vingenerator;

import com.vingenerator.domain.VINCode;
import com.vingenerator.repos.VINrepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class WebGeneratorController {

    @Autowired
    GeneratorService generatorService;

    @Autowired
    VINrepository vinRepository;

    @GetMapping("/main")
    public String main(Map<String, Object> model) {
        Iterable<VINCode> codes = vinRepository.findAll();
        model.put("codes", codes);
        model.put("code", generatorService.generateVINCode());
        return "main";
    }

    @PostMapping
    public String generateVIN(Map<String, Object> model){
        VINCode vinCode = new VINCode(generatorService.generateVINCode());
        vinRepository.save(vinCode);
        Iterable<VINCode> codes = vinRepository.findAll();
        model.put("codes", codes);
        return "main";
    }

}
