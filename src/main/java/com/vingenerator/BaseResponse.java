package com.vingenerator;

public class BaseResponse {

    private final String status;

    public BaseResponse(String status){
        this.status = status;
    }

    public String getStatus(){
        return status;
    }
}
